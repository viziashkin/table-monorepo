# LoremIpsum

## Install

```
npm i
```

## Run

Just in case anything doesn't work or just to save some time, here is 1-minute demo:

- https://1drv.ms/u/s!AilRqqg3HxO-h-NvMTtYr3RPujCpJw?e=0yX1nF (size: 8MB)

### FE

```
nx serve lorem-angular
or
nx serve lorem-angular --port <port>

```

### BE

```
nx serve lorem-express
or
nx serve lorem-express --port <port>
```

But be aware that url is hardcoded, in case you specify different port - change that in `apps\lorem-angular\src\app\api\api.service.ts`

## Description

Talking about architecture, it's Nx monorepo that allows to have several apps or fullstack app in a single repo, logically.
Also features ESLint installed, husky pre-commit hook that runs prettier and eslit.

## FE Structure

### Shared components

Set of shared componens library-alike, bundled in a single module (`apps\lorem-angular\src\app\ui-components`)

1. **Button** - just a directive to apply default class and remove focus on click
2. **Form-field** - custom form-field to handle label, error message, icon
3. **Icon** - a simple component that accepts icon name and takes an svg from `apps\lorem-angular\src\assets\symbol-defs.svg`
4. **Modal** - a bunch of guys enables the app with a laconic popup including:

- _modal.component_ - base component with overlay, close btn, animation that dynamically accepts componentRef and some context data and renders it.
- _modal.directive_ - that's how modal understand where to place compoenent
- _modal-info.component_ - predefined template of modal with some text data and a single button, results with a just close event
- _modal-confirmation.component_ - predefined tempalte of modal that can have some text data and two buttons, as well results with 2 types of events based on button pressed
- _modal.service_ - main logic happens here. Each modal returns an Observable converted from Subject to provide close/cancel/confirm event. It is also possible dynamically to change some config of the modal like if it should have close btn
- _modal.model_ - some typing so it wouldn't blow up

5. **Table** - just a dynamically generated table that has some controls that emit events thorugh component output
6. **Theme** variables - some colors and global styles, mixins, breakpoints (`apps\lorem-angular\src\app\theme\variables.scss`)

### API

Just a service working with httpClient (`apps\lorem-angular\src\app\api`)

### Store

A service implementing a store for main table data via BehavioralSubject
(`apps\lorem-angular\src\app\table-view\table-view-store.service.ts`)

### Main view

It is lazy loaded module with own routing.

1. _Table-view.component_ - main component holding the table and some controls, reacts to table controls, handles some additional routing, moslty is just a middleware betwee table and _table-view.service_
2. _Table-view.service_ - the most of logic happens here like requesting data from API, displaying all the popups and parsing response data to tableHeaders and tableRecords
3. _Table.edit-modal_ - a template of modal that allows to create or edit a record. Dynamically passed to _modal.component_. Generates controls for any array of fields, has basic validation (required).

## BE

A simple Express app that use mongoose to connect to a cluster with a MongoDB hosted. Contains a simple controller and api endpoints.
URL is located here: `apps\lorem-express\src\app\database\db.ts`

Data model:

```
{
  id: string,
  field1: string,
  field2: string,
  field3: string,
  field4: string,
  field5: string,
}
```

Quite simple, but with a minor changes the table should work with any data model.
