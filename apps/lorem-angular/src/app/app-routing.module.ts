import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'table',
    loadChildren: () =>
      import('./table-view/table-view.module').then((m) => m.TableViewModule),
  },
  {
    path: 'table/:page',
    loadChildren: () =>
      import('./table-view/table-view.module').then((m) => m.TableViewModule),
  },
  {
    path: '**',
    redirectTo: 'table',
    pathMatch: 'full',
  },
  {
    path: '',
    redirectTo: 'table',
    pathMatch: 'full',
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
