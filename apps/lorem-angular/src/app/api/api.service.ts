import { ILoremIpsum, LoremIpsumResponse } from './api.model';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import {
  ITableContentData,
  ITableField,
  ITableHeader,
  ITableHeaderData,
  ITableRecord,
} from '../ui-components/table/table.model';

@Injectable({
  providedIn: 'root',
})
export class ApiService {
  constructor(private http: HttpClient) {}

  baseUrl = 'http://localhost:3333';

  getLoremData(): Observable<LoremIpsumResponse> {
    return this.http.get<LoremIpsumResponse>(`${this.baseUrl}/api/getAll`);
  }

  getLoremSingle(id: string): Observable<LoremIpsumResponse> {
    return this.http.get<LoremIpsumResponse>(
      `${this.baseUrl}/api/getSingle/${id}`
    );
  }

  postLorem(lorem: ILoremIpsum): Observable<LoremIpsumResponse> {
    return this.http.post<LoremIpsumResponse>(
      `${this.baseUrl}/api/create`,
      lorem
    );
  }

  postLoremMultiple(loremArr: ILoremIpsum[]): Observable<LoremIpsumResponse> {
    return this.http.post<LoremIpsumResponse>(
      `${this.baseUrl}/api/createMultiple`,
      loremArr
    );
  }

  putLorem(lorem: ILoremIpsum, id: string): Observable<LoremIpsumResponse> {
    return this.http.put<LoremIpsumResponse>(
      `${this.baseUrl}/api/update/${id}`,
      lorem
    );
  }

  deleteLorem(id: string): Observable<unknown> {
    return this.http.delete<LoremIpsumResponse>(
      `${this.baseUrl}/api/delete/${id}`
    );
  }

  deleteLoremAll(): Observable<unknown> {
    return this.http.delete<LoremIpsumResponse>(
      `${this.baseUrl}/api/deleteAll`
    );
  }

  generateSampleLoremIpsum(count: number = 3): LoremIpsumResponse {
    const loremIpsum: LoremIpsumResponse = [];

    for (let i = 0; i < count; i++) {
      const lorem: ILoremIpsum = {
        id: i.toString(),
        field1: 'field1 VAL',
        field2: 'field1 VAL',
        field3: 'field1 VAL',
        field4: 'field1 VAL',
        field5: 'field1 VAL',
      };
      loremIpsum.push(lorem);
    }

    return loremIpsum;
  }

  generateSampleHeaderData(fieldsNum) {
    const headerData: ITableHeaderData = [];
    for (let i = 0; i < fieldsNum; i++) {
      const headerItem: ITableHeader = {
        name: `fieldName${i}`,
        displayText: `header${i} VAL`,
      };
      headerData.push(headerItem);
    }
    return headerData;
  }

  generateSampleContentData(fieldsNum, recordsNum) {
    const records: ITableRecord[] = [];

    for (let i = 0; i < recordsNum; i++) {
      const fields: ITableField[] = [];
      for (let n = 0; n < fieldsNum; n++) {
        const field: ITableField = {
          name: `field${n}`,
          textContent: `field${n} record${i} VAL`,
        };
        fields.push(field);
      }

      const record: ITableRecord = {
        id: i.toString(),
        value: fields,
      };
      records.push(record);
    }

    const contentData: ITableContentData = {
      records: records,
    };

    return contentData;
  }
}
