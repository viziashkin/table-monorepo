export interface ILoremIpsum {
  id: string;
  field1: string;
  field2: string;
  field3: string;
  field4: string;
  field5: string;
}

export type LoremIpsumResponse = Array<ILoremIpsum>;
