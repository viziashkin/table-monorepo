export interface Lorem {
  id?: string;
  field1: string;
  field2: string;
  field3: string;
  field4: string;
  field5: string;
}

export enum ETableViewRoutes {
  CREATE = 'create',
  EDIT = 'edit',
  DELETE = 'delete',
  BASE = 'table',
}
