import { TableViewComponent } from './table-view.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ETableViewRoutes } from './table-view-model';

const routes: Routes = [
  {
    path: '',
    component: TableViewComponent,
  },
  {
    path: `${ETableViewRoutes.CREATE}`,
    component: TableViewComponent,
  },
  {
    path: `${ETableViewRoutes.EDIT}/:id`,
    component: TableViewComponent,
  },
  {
    path: `${ETableViewRoutes.DELETE}/:id`,
    component: TableViewComponent,
  },
  {
    path: '**',
    redirectTo: 'view',
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TableViewRoutingModule {}
