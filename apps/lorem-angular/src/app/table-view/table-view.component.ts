import { TableViewStoreService } from './table-view-store.service';
import { ETableViewRoutes } from './table-view-model';
import { FormControl, FormGroup } from '@angular/forms';
import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TableViewService } from './table-view.service';
import {
  ETableControl,
  ITableContentData,
  ITableControlEvent,
  ITableHeaderData,
  ITableRecordControlEvent,
} from '../ui-components/table/table.model';
import { combineLatest, Subscription } from 'rxjs';
import { map, takeWhile } from 'rxjs/operators';
import { Location } from '@angular/common';

const sampleControls: ETableControl[] = [
  ETableControl.EDIT,
  ETableControl.DELETE,
];

@Component({
  selector: 'lorem-table-view',
  templateUrl: './table-view.component.html',
  styleUrls: ['./table-view.component.scss'],
})
export class TableViewComponent {
  form = new FormGroup({ field1: new FormControl() });

  dataSubscription$: Subscription;

  tableHeaderData: ITableHeaderData;
  tableContentData: ITableContentData;
  tableControls: ETableControl[] = sampleControls;

  modalCloseSub: Subscription;
  routeSub: Subscription;

  constructor(
    private store: TableViewStoreService,
    private tableViewService: TableViewService,
    private activatedroute: ActivatedRoute,
    private location: Location
  ) {
    this.tableHeaderData = this.tableViewService.generateDefaultLoremHeaderData();
    this.subscribeToData();
    this.tableViewService.getLoremData();
    this.handleRouting();
  }

  subscribeToData(): void {
    this.dataSubscription$ = this.store.loremData$.subscribe((response) => {
      if (response && response?.length) {
        this.tableHeaderData = this.tableViewService.parseToTableHeaderData(
          response
        );
        this.tableContentData = this.tableViewService.parseToTableContentData(
          response
        );
      } else {
        this.tableContentData = { records: [] };
      }
    });
  }

  handleRouting() {
    this.routeSub = combineLatest([
      this.activatedroute.url,
      this.activatedroute.params,
      this.store.loremData$,
    ])
      .pipe(
        map(([url, params, contentData]) => ({ url, params, contentData })),
        takeWhile((routeData) => {
          return routeData.contentData && !!routeData.contentData.length;
        }, true)
      )
      .subscribe((routeData) => {
        const segment = routeData.url[0]?.path;
        const id = routeData.params?.id;

        switch (segment) {
          case ETableViewRoutes.CREATE:
            this.tableViewService.handleRecordCreateEvent(this.tableHeaderData);
            break;
          case ETableViewRoutes.EDIT:
            this.tableViewService.handleRecordEditEvent(id);
            break;
          case ETableViewRoutes.DELETE:
            this.tableViewService.handleRecordDeleteEvent(id);
            break;
        }
      });
  }

  handleTableRowControlClick(event: ITableRecordControlEvent): void {
    switch (event.eventType) {
      case ETableControl.EDIT:
        this.tableViewService.handleRecordEditEvent(event.recordId);
        break;
      case ETableControl.DELETE:
        this.tableViewService.handleRecordDeleteEvent(event.recordId);
        break;
      default:
        break;
    }
  }

  handleTableControlClicked(event: ITableControlEvent): void {
    switch (event.eventType) {
      case ETableControl.CREATE:
        this.tableViewService.handleRecordCreateEvent(this.tableHeaderData);
        break;
      default:
        break;
    }
  }

  onGenerateRecordsClick(): void {
    this.tableViewService.createLoremMultiple(
      this.tableViewService.generateMultipleLorem(10)
    );
  }

  onDeleteAllClick(): void {
    this.tableViewService.deleteLoremAll();
  }
}
