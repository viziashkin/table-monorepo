import { UiComponentsModule } from './../ui-components/ui-components.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TableViewRoutingModule } from './table-view-routing.module';
import { TableViewComponent } from './table-view.component';
import { ReactiveFormsModule } from '@angular/forms';
import { TableEditModalComponent } from './table-edit-modal/table-edit-modal.component';

@NgModule({
  declarations: [TableViewComponent, TableEditModalComponent],
  imports: [
    CommonModule,
    TableViewRoutingModule,
    UiComponentsModule,
    ReactiveFormsModule,
  ],
})
export class TableViewModule {}
