import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { LoremIpsumResponse } from '../api/api.model';

@Injectable({
  providedIn: 'root',
})
export class TableViewStoreService {
  private readonly _loremData = new BehaviorSubject<LoremIpsumResponse>([]);
  readonly loremData$ = this._loremData.asObservable();

  private get loremData(): LoremIpsumResponse {
    return this._loremData.getValue();
  }

  private set loremData(val: LoremIpsumResponse) {
    this._loremData.next(val);
  }

  updateLoremData(data: LoremIpsumResponse) {
    this.loremData = data;
  }

  constructor() {}
}
