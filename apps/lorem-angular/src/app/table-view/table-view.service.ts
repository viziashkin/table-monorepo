import { ActivatedRoute, Router } from '@angular/router';
import { ModalInfoComponent } from './../ui-components/modal/modal-info/modal-info.component';
import { Observable } from 'rxjs';
import { first } from 'rxjs/operators';
import { ApiService } from './../api/api.service';
import { ITableEditModalData } from './table-edit-modal/table-edit-modal-model';
import { TableEditModalComponent } from './table-edit-modal/table-edit-modal.component';
import { TableViewStoreService } from './table-view-store.service';
import {
  ITableContentData,
  ITableField,
  ITableHeader,
  ITableHeaderData,
  ITableRecord,
} from './../ui-components/table/table.model';
import {
  ICoonfirmationModalData,
  EConfirmationModalResult,
  IInfoModalData,
} from './../ui-components/modal/modal.model';
import { ModalConfirmationComponent } from './../ui-components/modal/modal-confirmation/modal-confirmation.component';
import { Injectable } from '@angular/core';
import { ModalService } from '../ui-components/modal/modal.service';
import { ILoremIpsum, LoremIpsumResponse } from '../api/api.model';
import { Location } from '@angular/common';
import { ETableViewRoutes } from './table-view-model';

@Injectable({
  providedIn: 'root',
})
export class TableViewService {
  constructor(
    private store: TableViewStoreService,
    private api: ApiService,
    private modalService: ModalService,
    private location: Location,
    private activatedRoute: ActivatedRoute,
    private router: Router
  ) {
    this.modalService.closeSubject.subscribe(() => {
      this.resetLocation();
    });
  }

  getLoremData() {
    this.api.getLoremData().subscribe(
      (response) => {
        this.store.updateLoremData(response);
      },
      (error) => {
        this.handleApiError(error);
      }
    );
  }

  getLoremSingle(id: string): Observable<LoremIpsumResponse> {
    return this.api.getLoremSingle(id);
  }

  createLorem(lorem: ILoremIpsum) {
    this.api.postLorem(lorem).subscribe(
      (res) => {
        const modalData: IInfoModalData = {
          headerText: 'Success',
          contentText: 'Record has been added',
          cancelBtnLabel: 'OK',
        };
        this.modalService.displayInfoModal(ModalInfoComponent, modalData);
        this.getLoremData();
      },
      (error) => {
        this.handleApiError(error);
      }
    );
  }

  createLoremMultiple(loremArr: ILoremIpsum[]) {
    this.api.postLoremMultiple(loremArr).subscribe(
      (res) => {
        const modalData: IInfoModalData = {
          headerText: 'Success',
          contentText: 'Multiple records has been added',
          cancelBtnLabel: 'OK',
        };
        this.modalService.displayInfoModal(ModalInfoComponent, modalData);
        this.getLoremData();
      },
      (error) => {
        this.handleApiError(error);
      }
    );
  }

  updateLorem(lorem: ILoremIpsum, id: string) {
    this.api.putLorem(lorem, id).subscribe(
      (res) => {
        const modalData: IInfoModalData = {
          headerText: 'Success',
          contentText: 'Record has been updated',
          cancelBtnLabel: 'OK',
        };
        this.getLoremData();
        this.modalService.displayInfoModal(ModalInfoComponent, modalData);
      },
      (error) => {
        this.handleApiError(error);
      }
    );
  }

  deleteLorem(id: string) {
    this.api.deleteLorem(id).subscribe(
      () => {
        const modalData: IInfoModalData = {
          headerText: 'Success',
          contentText: 'Record has been deleted',
          cancelBtnLabel: 'OK',
        };
        this.getLoremData();
        this.modalService
          .displayInfoModal(ModalInfoComponent, modalData)
          .pipe(first())
          .subscribe(() => {
            this.resetLocation();
          });
      },
      (error) => {
        this.handleApiError(error);
      }
    );
  }

  deleteLoremAll() {
    this.api.deleteLoremAll().subscribe(
      () => {
        const modalData: IInfoModalData = {
          headerText: 'Success',
          contentText: 'All records has been deleted',
          cancelBtnLabel: 'OK',
        };
        this.getLoremData();
        this.modalService
          .displayInfoModal(ModalInfoComponent, modalData)
          .pipe(first())
          .subscribe(() => {
            this.resetLocation();
          });
      },
      (error) => {
        this.handleApiError(error);
      }
    );
  }

  handleApiError(error): void {
    const modalData: IInfoModalData = {
      headerText: 'Error',
      contentText: error.error.message,
      cancelBtnLabel: 'OK',
    };
    this.modalService.displayInfoModal(ModalInfoComponent, modalData);
  }

  handleRecordCreateEvent(headersData: ITableHeaderData): void {
    const modalData: ITableEditModalData = {
      recordId: null,
      isEdit: false,
      headerData: headersData,
    };
    this.changeLocation(`${ETableViewRoutes.BASE}/${ETableViewRoutes.CREATE}`);
    this.modalService
      .displayModal(TableEditModalComponent, modalData)
      .pipe(first())
      .subscribe(() => {
        this.resetLocation();
      });
  }

  handleRecordEditEvent(recordId: string): void {
    const modalData: ITableEditModalData = {
      recordId: recordId,
      isEdit: true,
    };
    this.changeLocation(
      `${ETableViewRoutes.BASE}/${ETableViewRoutes.EDIT}/${recordId}`
    );
    this.modalService
      .displayModal(TableEditModalComponent, modalData)
      .pipe(first())
      .subscribe(() => {
        this.resetLocation();
      });
  }

  handleRecordDeleteEvent(recordId: string): void {
    const confirmationModalData: ICoonfirmationModalData = {
      headerText: 'You are going to delete a record. Are you sure?',
      contentText: `Record id: ${recordId}`,
      confirmBtnLabel: 'Delete',
      cancelBtnLabel: 'Back',
    };
    this.changeLocation(
      `${ETableViewRoutes.BASE}/${ETableViewRoutes.DELETE}/${recordId}`
    );
    this.modalService
      .displayConfirmationModal(
        ModalConfirmationComponent,
        confirmationModalData
      )
      .pipe(first())
      .subscribe((EConfirmationModalResult) => {
        this.handleDeleteConfirmationModalResult(
          EConfirmationModalResult,
          recordId
        );
      });
  }

  handleDeleteConfirmationModalResult(
    result: EConfirmationModalResult,
    recordId: string
  ): void {
    switch (result) {
      case EConfirmationModalResult.CONFIRM:
        this.deleteLorem(recordId);
        break;
      default:
        break;
    }
  }

  changeLocation(path: string): void {
    const segments = window.location.href.split('/');
    const lastSegment = segments[segments.length - 1];

    if (lastSegment === ETableViewRoutes.BASE) {
      this.location.go(path);
    }
  }

  resetLocation(): void {
    this.location.go(`/${ETableViewRoutes.BASE}`);
  }

  parseToTableHeaderData(response: LoremIpsumResponse): ITableHeaderData {
    const item = response[0];
    const tableHeaders: ITableHeaderData = [];
    Object.keys(item).forEach((key) => {
      if (key !== 'id') {
        const tableHeader: ITableHeader = {
          name: key,
          displayText: key,
        };
        tableHeaders.push(tableHeader);
      }
    });
    return tableHeaders;
  }

  parseToTableContentData(response: LoremIpsumResponse): ITableContentData {
    const data = response;
    const tableContentData: ITableContentData = { records: [] };
    const records: ITableRecord[] = [];

    data.forEach((recordRaw) => {
      records.push(this.parseSingleRecord(recordRaw));
    });

    tableContentData.records = records;
    return tableContentData;
  }

  parseSingleRecord(recordRaw: ILoremIpsum) {
    const fields: ITableField[] = [];
    for (const [key, value] of Object.entries(recordRaw)) {
      if (key !== 'id') {
        const field: ITableField = {
          name: key,
          textContent: value,
        };
        fields.push(field);
      }
    }

    const record = {
      id: recordRaw.id,
      value: fields,
    };

    return record;
  }

  generateDefaultLoremHeaderData(): ITableHeaderData {
    const emptyLoremIpsum: ILoremIpsum = {
      id: '',
      field1: '',
      field2: '',
      field3: '',
      field4: '',
      field5: '',
    };
    return this.parseToTableHeaderData([emptyLoremIpsum]);
  }

  generateMultipleLorem(qty: number): ILoremIpsum[] {
    const loremArr: ILoremIpsum[] = [];
    for (let i = 0; i < qty; i++) {
      const lorem: ILoremIpsum = {
        id: '',
        field1: 'val ' + i + 1,
        field2: 'val ' + i + 2,
        field3: 'val ' + i + 3,
        field4: 'val ' + i + 4,
        field5: 'val ' + i + 5,
      };
      loremArr.push(lorem);
    }
    return loremArr;
  }
}
