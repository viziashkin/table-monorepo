import { TestBed } from '@angular/core/testing';

import { TableViewStoreService } from './table-view-store.service';

describe('TableViewStoreService', () => {
  let service: TableViewStoreService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TableViewStoreService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
