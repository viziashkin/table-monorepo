import { LoremIpsumResponse } from './../../api/api.model';
import { TableViewService } from './../table-view.service';
import { ModalService } from './../../ui-components/modal/modal.service';
import { ITableEditModalData } from './table-edit-modal-model';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ITableHeaderData } from './../../ui-components/table/table.model';
import { Component, OnInit } from '@angular/core';
import { ILoremIpsum } from '../../api/api.model';

@Component({
  selector: 'lorem-table-edit-modal',
  templateUrl: './table-edit-modal.component.html',
  styleUrls: ['./table-edit-modal.component.scss'],
})
export class TableEditModalComponent implements OnInit {
  set data(val: ITableEditModalData) {
    this.recordId = val.recordId;
    this.isEdit = val.isEdit;
    if (val.headerData) this.tableHeaders = val.headerData;
  }

  recordId: string;
  form: FormGroup;
  isEdit: boolean;

  tableHeaders: ITableHeaderData;
  lorem: LoremIpsumResponse;

  constructor(
    private modalService: ModalService,
    private tableViewService: TableViewService
  ) {}

  loadRecord() {
    this.tableViewService.getLoremSingle(this.recordId).subscribe(
      (response) => {
        this.lorem = response;
        this.initForm();
      },
      (error) => {
        this.tableViewService.handleApiError(error);
      }
    );
  }

  onFormSubmit(): void {
    if (!this.form.valid) {
      Object.values(this.form.controls).forEach((control) => {
        control.markAsDirty();
      });
      return;
    }

    const newRecordValue = this.form.value;
    const id = this.recordId || '-1';

    const newLorem: ILoremIpsum = {
      id: id,
      field1: newRecordValue['field1'],
      field2: newRecordValue['field2'],
      field3: newRecordValue['field3'],
      field4: newRecordValue['field4'],
      field5: newRecordValue['field5'],
    };

    if (this.isEdit) {
      this.tableViewService.updateLorem(newLorem, id);
    } else {
      this.tableViewService.createLorem(newLorem);
    }
  }

  onCancelClick(): void {
    this.modalService.closeModal();
  }

  initForm(): void {
    if (!this.form) {
      this.form = new FormGroup(this.getFormControls(true));
    }
  }

  getFormControls(returnObject: boolean): any {
    if (returnObject) {
      const controlsObj = {};
      if (this.lorem) {
        for (const [key, value] of Object.entries(this.lorem)) {
          controlsObj[key] = new FormControl(value, [Validators.required]);
        }
      } else {
        this.tableHeaders.forEach((header) => {
          controlsObj[header.name] = new FormControl('', [Validators.required]);
        });
      }

      return controlsObj;
    } else {
      const controlsArr = [];

      if (this.lorem) {
        for (const [key] of Object.entries(this.lorem)) {
          controlsArr.push(key);
        }
      } else {
        this.tableHeaders.forEach((header) => {
          controlsArr.push(header.name);
        });
      }

      return controlsArr;
    }
  }

  ngOnInit(): void {
    if (this.isEdit) {
      this.loadRecord();
    } else {
      this.initForm();
    }
  }
}
