import { ITableHeaderData } from './../../ui-components/table/table.model';

export interface ITableEditModalData {
  recordId: string;
  isEdit: boolean;
  headerData?: ITableHeaderData;
}
