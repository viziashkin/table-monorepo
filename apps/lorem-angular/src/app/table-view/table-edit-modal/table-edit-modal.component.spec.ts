import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TableEditModalComponent } from './table-edit-modal.component';

describe('TableEditModalComponent', () => {
  let component: TableEditModalComponent;
  let fixture: ComponentFixture<TableEditModalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [TableEditModalComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TableEditModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
