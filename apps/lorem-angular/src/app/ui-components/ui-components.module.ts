import { ModalModule } from './modal/modal.module';
import { TableModule } from './table/table.module';
import { IconModule } from './icon/icon.module';
import { ButtonModule } from './button/button.module';
import { FormFieldModule } from './form-field/form-field.module';

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

@NgModule({
  imports: [
    CommonModule,
    FormFieldModule,
    ButtonModule,
    IconModule,
    TableModule,
    ModalModule,
  ],
  exports: [
    FormFieldModule,
    ButtonModule,
    IconModule,
    TableModule,
    ModalModule,
  ],
})
export class UiComponentsModule {}
