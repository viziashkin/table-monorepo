import { Directive, ViewContainerRef } from '@angular/core';

@Directive({
  selector: '[uiModalHost]',
})
export class ModalDirective {
  constructor(public viewContainerRef: ViewContainerRef) {}
}
