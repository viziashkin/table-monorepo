import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import {
  EConfirmationModalResult,
  ICoonfirmationModalData,
  IDynamicModalComponentData,
  IModalConfig,
  ModalData,
} from './modal.model';

@Injectable({
  providedIn: 'root',
})
export class ModalService {
  dataSubject = new Subject<IDynamicModalComponentData>();
  configSubject = new Subject<IModalConfig>();
  closeSubject = new Subject<EConfirmationModalResult>();
  timer: any;
  confirmationModalResult = new Subject<EConfirmationModalResult>();

  constructor() {}

  setCloseBtnColor(isDark: boolean): void {
    this.configSubject.next({ isCloseBtnDark: isDark });
  }

  toggleCloseBtn(isVisible: boolean): void {
    this.configSubject.next({ isCloseBtnVisible: isVisible });
  }

  displayModal(
    component: any,
    data: ModalData
  ): Observable<EConfirmationModalResult> {
    clearTimeout(this.timer);
    this.dataSubject.next({ component: component, data: data });
    return this.closeSubject.asObservable();
  }

  displayInfoModal(
    component: any,
    data: ModalData
  ): Observable<EConfirmationModalResult> {
    clearTimeout(this.timer);
    this.dataSubject.next({ component: component, data: data });
    return this.closeSubject.asObservable();
  }

  displayConfirmationModal(
    component: any,
    data: ICoonfirmationModalData
  ): Observable<EConfirmationModalResult> {
    clearTimeout(this.timer);
    this.dataSubject.next({
      component: component,
      data: data,
    });
    return this.confirmationModalResult.asObservable();
  }

  handleConfirmAction(): void {
    this.confirmationModalResult.next(EConfirmationModalResult.CONFIRM);
  }

  handleCancelAction(): void {
    this.confirmationModalResult.next(EConfirmationModalResult.CANCEL);
    this.closeModal();
  }

  closeModal(isDelayed = false, timeout = 0): void {
    if (isDelayed) {
      this.timer = setTimeout(() => {
        this.closeModal();
      }, timeout);
    } else {
      clearTimeout(this.timer);
      this.dataSubject.next({ component: null });
      this.closeSubject.next(EConfirmationModalResult.DISMISS);
      this.confirmationModalResult.next(EConfirmationModalResult.DISMISS);
    }
  }

  getDataSubject(): Observable<IDynamicModalComponentData> {
    return this.dataSubject.asObservable();
  }

  getConfigSubject(): Observable<IModalConfig> {
    return this.configSubject.asObservable();
  }

  getOnCloseSubject(): Observable<EConfirmationModalResult> {
    return this.closeSubject.asObservable();
  }
}
