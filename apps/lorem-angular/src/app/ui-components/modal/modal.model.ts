export interface ICoonfirmationModalData {
  headerText: string;
  contentText: string;
  confirmBtnLabel?: string;
  cancelBtnLabel?: string;
  context?: unknown;
}

export interface IInfoModalData {
  headerText: string;
  contentText: string;
  cancelBtnLabel?: string;
  context?: unknown;
}

export type ModalData = ICoonfirmationModalData | IInfoModalData | any;

export interface IDynamicModalComponentData {
  component: unknown;
  data?: ModalData;
}

export interface IModalConfig {
  isCloseBtnVisible?: boolean;
  isCloseBtnDark?: boolean;
}

export enum EConfirmationModalResult {
  CONFIRM,
  CANCEL,
  DISMISS,
}
