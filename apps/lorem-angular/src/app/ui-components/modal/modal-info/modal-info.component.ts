import { Component } from '@angular/core';
import { IInfoModalData } from '../modal.model';
import { ModalService } from '../modal.service';

@Component({
  selector: 'lorem-modal-info',
  templateUrl: './modal-info.component.html',
  styleUrls: ['./modal-info.component.scss'],
})
export class ModalInfoComponent implements IInfoModalData {
  set data(val: IInfoModalData) {
    this.headerText = val.headerText;
    this.contentText = val.contentText;
    this.cancelBtnLabel = val.cancelBtnLabel;
    this.context = val.context;
  }

  headerText: string;
  contentText: string;
  cancelBtnLabel = 'Confirm';
  context: unknown;
  constructor(private modalService: ModalService) {}

  onCancelClick(): void {
    this.modalService.closeModal();
  }
}
