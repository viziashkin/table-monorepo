import { IDynamicModalComponentData, ModalData } from './modal.model';
import { ModalService } from './modal.service';
import { ModalDirective } from './modal.directive';
import {
  Component,
  ComponentFactoryResolver,
  ComponentRef,
  HostListener,
  ViewChild,
} from '@angular/core';
import { Subscription } from 'rxjs';
import {
  animate,
  state,
  style,
  transition,
  trigger,
} from '@angular/animations';

const animationTimeMs = 200;

@Component({
  selector: 'ui-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss'],
  animations: [
    trigger('openAnimationModal', [
      state('0', style({ opacity: 0, transform: 'scale(0.5)' })),
      state('1', style({ opacity: 1, transform: 'scale(1)' })),
      transition('0 => 1', animate(`${animationTimeMs}ms ease-out`)),
      transition('1 => 0', animate(`${animationTimeMs}ms ease-in`)),
    ]),
    trigger('openAnimationOverlay', [
      state('0', style({ opacity: 0 })),
      state('1', style({ opacity: 0.6 })),
      transition('0 => 1', animate(`${animationTimeMs}ms ease-out`)),
      transition('1 => 0', animate(`${animationTimeMs}ms ease-in`)),
    ]),
  ],
})
export class ModalComponent {
  isOpen = false;
  isOpenAnimationState = false;
  isCloseBtnDark = true;
  isCloseBtnVisible = true;
  dataSubscription: Subscription;
  configSubscription: Subscription;
  componentRef: ComponentRef<unknown>;

  @ViewChild(ModalDirective, { static: true }) modalHost: ModalDirective;

  @HostListener('document:keyup', ['$event'])
  handleKeyboardEvent(event: KeyboardEvent) {
    if (event.code === 'Escape') {
      this.closeSelf();
    }
  }

  constructor(
    private componentFactoryResolver: ComponentFactoryResolver,
    private modalService: ModalService
  ) {
    this.dataSubscription = this.modalService
      .getDataSubject()
      .subscribe((result) => {
        if (result?.component) {
          this.openSelf(result);
        } else {
          this.closeSelf();
        }
      });

    this.configSubscription = this.modalService
      .getConfigSubject()
      .subscribe((result) => {
        if (result?.isCloseBtnDark !== undefined)
          this.isCloseBtnDark = result.isCloseBtnDark;
        if (result?.isCloseBtnVisible !== undefined)
          this.isCloseBtnVisible = result.isCloseBtnVisible;
      });
  }

  loadComponent(component: any, data: ModalData): void {
    const componentFactory = this.componentFactoryResolver.resolveComponentFactory(
      component
    );

    const viewContainerRef = this.modalHost.viewContainerRef;
    viewContainerRef.clear();

    this.componentRef = viewContainerRef.createComponent(componentFactory);
    if (data) this.componentRef.instance['data'] = data;
  }

  onOverlayClick(): void {
    this.closeSelf();
  }

  onCloseBtnClick(): void {
    this.closeSelf();
  }

  openSelf(context: IDynamicModalComponentData): void {
    this.isCloseBtnDark = true;
    this.isCloseBtnVisible = true;
    this.loadComponent(context.component, context.data);
    this.isOpen = true;
    this.isOpenAnimationState = true;
  }

  closeSelf(): void {
    if (!this.isOpen) return;
    this.isOpenAnimationState = false;

    setTimeout(() => {
      this.isOpen = false;
      this.componentRef.destroy();
      this.modalHost.viewContainerRef.clear();
      this.modalService.closeModal();
    }, animationTimeMs);
  }
}
