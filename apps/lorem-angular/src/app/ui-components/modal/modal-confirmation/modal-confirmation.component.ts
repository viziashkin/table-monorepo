import { ModalService } from './../modal.service';
import { ICoonfirmationModalData } from './../modal.model';
import { Component } from '@angular/core';

@Component({
  selector: 'lorem-modal-confirmation',
  templateUrl: './modal-confirmation.component.html',
  styleUrls: ['./modal-confirmation.component.scss'],
})
export class ModalConfirmationComponent implements ICoonfirmationModalData {
  set data(val: ICoonfirmationModalData) {
    this.headerText = val.headerText;
    this.contentText = val.contentText;
    this.confirmBtnLabel = val.confirmBtnLabel;
    this.cancelBtnLabel = val.cancelBtnLabel;
    this.context = val.context;
  }

  headerText: string;
  contentText: string;
  confirmBtnLabel = 'Confirm';
  cancelBtnLabel = 'Cancel';
  context: unknown;

  constructor(private modalService: ModalService) {}

  onCancelClick(): void {
    this.modalService.handleCancelAction();
  }

  onConfirmClick(): void {
    this.modalService.handleConfirmAction();
  }
}
