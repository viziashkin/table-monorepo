import { ButtonModule } from './../button/button.module';
import { IconModule } from './../icon/icon.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ModalComponent } from './modal.component';
import { ModalDirective } from './modal.directive';
import { ModalConfirmationComponent } from './modal-confirmation/modal-confirmation.component';
import { ModalInfoComponent } from './modal-info/modal-info.component';

@NgModule({
  declarations: [
    ModalComponent,
    ModalDirective,
    ModalConfirmationComponent,
    ModalInfoComponent,
  ],
  imports: [CommonModule, IconModule, ButtonModule],
  exports: [ModalComponent],
})
export class ModalModule {}
