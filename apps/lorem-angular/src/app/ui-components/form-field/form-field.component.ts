/* eslint-disable @angular-eslint/no-host-metadata-property */
import { InputDirective } from './input.directive';
import { FormGroup, NgControl } from '@angular/forms';
import {
  Component,
  ContentChild,
  Input,
  ViewEncapsulation,
} from '@angular/core';
import { coerceBooleanProperty } from '@angular/cdk/coercion';

@Component({
  selector: 'ui-form-field',
  templateUrl: './form-field.component.html',
  styleUrls: ['./form-field.component.scss'],
  host: {
    '[class.ui-form-field]': 'true',
    '[class.ng-untouched]': '_shouldForward("untouched")',
    '[class.ng-touched]': '_shouldForward("touched")',
    '[class.ng-pristine]': '_shouldForward("pristine")',
    '[class.ng-dirty]': '_shouldForward("dirty")',
    '[class.ng-valid]': '_shouldForward("valid")',
    '[class.ng-invalid]': '_shouldForward("invalid")',
    '[class.ng-pending]': '_shouldForward("pending")',
  },
  encapsulation: ViewEncapsulation.None,
})
export class FormFieldComponent {
  @Input() form: FormGroup;
  @Input() controlName: string;
  @Input() labelText: string;
  @Input() errorText: string;

  _displayError = true;
  get displayError(): boolean {
    return this._displayError;
  }
  @Input()
  set displayError(value: boolean) {
    this._displayError = coerceBooleanProperty(value);
  }

  @ContentChild(InputDirective) input: InputDirective;

  constructor() {}

  _shouldForward(prop: keyof NgControl): boolean {
    const ngControl = this.input.ngControl;
    return ngControl && ngControl[prop];
  }
}
