import { Directive, HostBinding } from '@angular/core';
import { NgControl } from '@angular/forms';

@Directive({
  selector: '[uiInput]',
})
export class InputDirective {
  @HostBinding('class.ui-input') get getDefaultClass(): boolean {
    return true;
  }

  constructor(public ngControl: NgControl) {}
}
