import {
  Directive,
  ElementRef,
  HostBinding,
  HostListener,
} from '@angular/core';

@Directive({
  selector: '[uiButton]',
})
export class ButtonDirective {
  @HostBinding('class.ui-button') get getDefaultClass(): boolean {
    return true;
  }

  @HostListener('click') onClick() {
    this.elRef.nativeElement.blur();
  }

  constructor(private elRef: ElementRef) {}
}
