import {
  ITableContentData,
  ITableHeaderData,
  ETableControl,
  ITableRecordControlEvent,
  ITableControlEvent,
} from './table.model';
import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'ui-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss'],
})
export class TableComponent {
  @Input() tableHeaders: ITableHeaderData;
  @Input() tableContent: ITableContentData;

  @Input() controls: ETableControl[] = [
    ETableControl.EDIT,
    ETableControl.DELETE,
  ];

  ETableControl = ETableControl;

  @Output()
  rowControlClicked: EventEmitter<ITableRecordControlEvent> = new EventEmitter<ITableRecordControlEvent>();

  @Output()
  controlClicked: EventEmitter<ITableControlEvent> = new EventEmitter<ITableControlEvent>();

  constructor() {}

  onRowControlClick(id: string, eventType: ETableControl): void {
    const tableEvent: ITableRecordControlEvent = {
      recordId: id,
      eventType: eventType,
    };
    this.rowControlClicked.emit(tableEvent);
  }

  onControlClick(eventType: ETableControl): void {
    this.controlClicked.emit({ eventType: eventType });
  }
}
