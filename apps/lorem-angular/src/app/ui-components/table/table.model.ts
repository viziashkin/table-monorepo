export interface ITableHeader {
  name: string;
  displayText: string;
}

export interface ITableField {
  name: string;
  textContent: string;
}

export interface ITableRecord {
  id: string;
  value: Array<ITableField>;
}

export type ITableHeaderData = Array<ITableHeader>;

export interface ITableContentData {
  records: ITableRecord[];
}

export enum ETableControl {
  EDIT = 'EDIT',
  DELETE = 'DELETE',
  CREATE = 'CREATE',
}

export type TableEvent = ETableControl;

export interface ITableControlEvent {
  eventType: TableEvent;
}

export interface ITableRecordControlEvent extends ITableControlEvent {
  recordId: string;
}
