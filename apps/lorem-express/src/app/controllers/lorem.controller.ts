import { LoremIpsum } from '../models/lorem';

function findAll(req, res) {
  LoremIpsum.find()
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message: err.message || 'Error occurred while retrieving records.',
      });
    });
}

function findSingle(req, res) {
  const id = req.params.id;

  LoremIpsum.findById(id)
    .then((data) => {
      if (!data)
        res.status(404).send({ message: 'Not found record with id ' + id });
      else res.send(data);
    })
    .catch((err) => {
      res
        .status(500)
        .send({ message: 'Error retrieving record with id=' + id });
    });
}

function createMultiple(req, res) {
  if (!req.body[0].field1) {
    res.status(400).send({ message: 'Invalid request' });
    return;
  }

  const documents = [];

  req.body.forEach((element) => {
    const lorem = new LoremIpsum({
      field1: element.field1,
      field2: element.field2,
      field3: element.field3,
      field4: element.field4,
      field5: element.field5,
    });
    documents.push(lorem);
  });

  LoremIpsum.insertMany(documents)
    .then((docs) => {
      res.send(docs);
    })
    .catch((error) => {
      res.status(500).send({ message: 'Error saving multiple records' });
    });
}

function create(req, res) {
  if (!req.body.field1) {
    res.status(400).send({ message: 'Invalid request' });
    return;
  }

  const lorem = new LoremIpsum({
    field1: req.body.field1,
    field2: req.body.field2,
    field3: req.body.field3,
    field4: req.body.field4,
    field5: req.body.field5,
  });

  lorem.save((err, doc) => {
    if (err) {
      res.status(500).send({
        message: err.message || 'Error occurred while creating the record.',
      });
    } else {
      res.send(doc);
    }
  });
}

function update(req, res) {
  if (!req.body) {
    return res.status(400).send({
      message: 'Data to update can not be empty!',
    });
  }

  const id = req.params.id;

  const lorem = {
    field1: req.body.field1,
    field2: req.body.field2,
    field3: req.body.field3,
    field4: req.body.field4,
    field5: req.body.field5,
  };

  LoremIpsum.findByIdAndUpdate(id, lorem, { useFindAndModify: false })
    .then((data) => {
      if (!data) {
        res.status(404).send({
          message: `Cannot update lorem with id=${id}.`,
        });
      } else res.send({ message: 'Was updated successfully.' });
    })
    .catch((err) => {
      res.status(500).send({
        message: 'Error updating lorem with id=' + id,
      });
    });
}

function deleteRecord(req, res) {
  const id = req.params.id;

  LoremIpsum.findByIdAndRemove(id, { useFindAndModify: false })
    .then((data) => {
      if (!data) {
        res.status(404).send({
          message: `Cannot delete record with id=${id}. Possibly record was not found.`,
        });
      } else {
        res.send({
          message: 'Record was deleted successfully.',
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        message: 'Could not delete record with id=' + id,
      });
    });
}

function deleteAll(req, res) {
  LoremIpsum.deleteMany({})
    .then(() => {
      res.send({
        message: 'All records were deleted successfully.',
      });
    })
    .catch(() => {
      res.status(500).send({
        message: 'Could not delete all records',
      });
    });
}

export {
  findAll,
  findSingle,
  create,
  createMultiple,
  update,
  deleteRecord,
  deleteAll,
};
