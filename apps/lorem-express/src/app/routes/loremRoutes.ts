import * as express from 'express';

import * as loremController from '../controllers/lorem.controller';

const router = express.Router();

router.get('/api/getAll', loremController.findAll);

router.get('/api/getSingle/:id', loremController.findSingle);

router.post('/api/create', loremController.create);

router.post('/api/createMultiple', loremController.createMultiple);

router.put('/api/update/:id', loremController.update);

router.delete('/api/delete/:id', loremController.deleteRecord);

router.delete('/api/deleteAll', loremController.deleteAll);

export { router as loremRouter };
