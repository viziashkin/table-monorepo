import * as mongoose from 'mongoose';

const loremSchema = new mongoose.Schema(
  {
    field1: {
      type: String,
      required: true,
    },
    field2: {
      type: String,
      required: true,
    },
    field3: {
      type: String,
      required: true,
    },
    field4: {
      type: String,
      required: true,
    },
    field5: {
      type: String,
      required: true,
    },
  },
  { collection: 'loremIpsum', versionKey: false }
);

loremSchema.method('toJSON', function () {
  const { _id, ...object } = this.toObject();
  object.id = _id;
  return object;
});

const loremModel = mongoose.model('loremIpsum', loremSchema);

export { loremModel as LoremIpsum };
